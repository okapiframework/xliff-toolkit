cd ../../
mvn clean install

cd deployment/maven
ant -f build_xliff-lib.xml

cd ../../applications/integration-tests
mvn -q integration-test
