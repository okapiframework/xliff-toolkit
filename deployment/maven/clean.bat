@echo off

if not exist ".\README.md" cd ..
if not exist ".\README.md" cd ..

call mvn clean -f applications/integration-tests
call ant clean -f deployment/maven
call mvn clean
