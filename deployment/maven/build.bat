@echo off

rem TODO: temporary file. To delete.
rem   Or add parameters, error detection, .sh equivalents, etc. (see okapi build file)

if not exist ".\README.md" cd ..
if not exist ".\README.md" cd ..

call mvn clean install
if ERRORLEVEL 1 goto end

call ant -f deployment/maven
if ERRORLEVEL 1 goto end

call mvn -q integration-test -f applications/integration-tests
if ERRORLEVEL 1 goto end

call mvn appengine:devserver -f applications/lynx-web/lynx-web-ear

:end
pause