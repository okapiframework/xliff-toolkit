The Okapi libraries are under Apache 2.0 License.

You can find the source code of this project at:
https://bitbucket.org/okapiframework/xliff-toolkit/src

For details and question about the Okapi XLIFF Toolkit, please go to:
https://bitbucket.org/okapiframework/xliff-toolkit
