### About Okapi Filters Plugin for OmegaT

The **Okapi XLIFF Toolkit** is a library that implements a set of Java components to create, read and manipulate XLIFF 2 documents.

**IMPORTANT: The library part (`okapi-lib-xliff2`) of this project has been integrated in the main
Okapi project (https://bitbucket.org/okapiframework/okapi/src/dev/okapi/filters/xliff2/)**  
**The current repo only contains the `lynx` (CLI) and `lynx-web` validation tools**

XLIFF 2 is being developed by the
[OASIS XLIFF TC](http://www.oasis-open.org/committees/tc_home.php?wg_abbrev=xliff).  
You can find more information [here](https://wiki.oasis-open.org/xliff/).  
The latest published version of the XLIFF 2 spec can be found at
https://docs.oasis-open.org/xliff/xliff-core/v2.1/xliff-core-v2.1.html

There is an introduction to XLIFF 2.0 [in this Multilingual article](https://multilingual.com/all-articles/?art_id=2139).

### Build status

`dev` branch: [![pipeline status](https://gitlab.com/okapiframework/xliff-toolkit/badges/dev/pipeline.svg)](https://gitlab.com/okapiframework/xliff-toolkit/commits/dev)

`master` branch: [![pipeline status](https://gitlab.com/okapiframework/xliff-toolkit/badges/master/pipeline.svg)](https://gitlab.com/okapiframework/xliff-toolkit/commits/master)

### Downloads and links

 * The latest stable version of the XLIFF Toolkit is at https://okapiframework.org/binaries/archives/Okapi_XLIFF_Toolkit/
 * The release Maven artifacts are here: https://mvnrepository.com/artifact/net.sf.okapi.lib/okapi-lib-xliff2/
 * You can now edit XLIFF 2 files in [OmegaT](http://www.omegat.org) using the [Okapi Filter Plugin](http://okapiframework.org/wiki/index.php?title=Okapi_Filters_Plugin_for_OmegaT#Support_for_XLIFF_2).
 * Get a [set of valid and invalid XLIFF 2 documents](https://tools.oasis-open.org/version-control/browse/wsvn/xliff/trunk/xliff-20/test-suite/) to test if a XLIFF 2 user agent is reading documents properly.
 * Read the [Getting Started guide](https://bitbucket.org/okapiframework/xliff-toolkit/wiki).
 * Read the [Java Documentation](http://okapiframework.org/xlifflib/javadoc/) of the library.
 * See also the tentative / prototyping object model for XLIFF and JSON serialization for XLIFF being worked on here: https://www.oasis-open.org/committees/xliff-omos.

### Developing with the API

The Java XLIFF2 library is available as an artifact in Maven Central.  
So all you need to do is add the dependency to your `pom.xml`:

```xml
  <!-- .... -->
  <dependencies>
    <dependency>
      <groupId>net.sf.okapi.lib</groupId>
      <artifactId>okapi-lib-xliff2</artifactId>
      <version>THE VERSION YOU WANT TO USE</version>
    </dependency>
  </dependencies>
```

To develop with the latest nightly snapshot build, you also need to add a custom repository:

```xml
  <repositories>
    <repository>
      <id>okapi-snapshot</id>
      <name>Okapi Snapshot</name>
      <url>https://oss.sonatype.org/content/repositories/snapshots/</url>
    </repository>
  </repositories>
  <!-- .... -->
  <dependencies>
    <dependency>
      <groupId>net.sf.okapi.lib</groupId>
      <artifactId>okapi-lib-xliff2</artifactId>
      <version>THE SNAPSHOT VERSION YOU WANT TO USE</version>
    </dependency>
  </dependencies>
```
