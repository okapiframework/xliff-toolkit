/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.applications.lynx;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.TransferHandler;

import net.sf.okapi.lib.xliff2.reader.XLIFFReader;

public class MainDialog extends JFrame {

	private static final long serialVersionUID = 1L;

    private JTextField edPath;
    private JButton btGetPath;
    private JButton btValidate;
    private JTextPane edLog;
    
    public MainDialog () {
        initComponents();
    }
    
    class DocumentTransferhandler extends TransferHandler {
    	
		private static final long serialVersionUID = 1L;
		
		@Override
		public boolean canImport (TransferHandler.TransferSupport support) {
            if ( !support.isDataFlavorSupported(DataFlavor.javaFileListFlavor) ) {
                return false;
            }
            boolean copySupported = (COPY & support.getSourceDropActions()) == COPY;
            if ( !copySupported ) {
                return false;
            }
            support.setDropAction(COPY);
            return true;
        }

		@Override
        public boolean importData (TransferHandler.TransferSupport support) {
            if ( !canImport(support) ) {
                return false;
            }
            Transferable t = support.getTransferable();
            try {
            	@SuppressWarnings("unchecked")
				java.util.List<File> l = (java.util.List<File>)t.getTransferData(DataFlavor.javaFileListFlavor);
                edPath.setText(l.get(0).getAbsolutePath());
            }
            catch ( Throwable e) {
            	return false;
            }
            return true;
        }    	
    }
     
    private void initComponents () {
    	setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
    	setTitle("Okapi Lynx - Validation and Testing Tool for XLIFF 2");
    	setLayout(new GridBagLayout());

    	JLabel lbPath = new JLabel("Enter, select or drag & drop the XLIFF document to process");
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        c.gridx = 0; c.gridwidth = 2;
        c.gridy = 0;
        c.insets = new Insets(5, 5, 5, 5);
    	add(lbPath, c);
    	
        edPath = new JTextField();
        edPath.requestFocusInWindow();
        c = new GridBagConstraints();
        c.anchor = GridBagConstraints.LINE_START;
        c.gridx = 0;
        c.gridy = 1;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 1.0;
        c.insets = new Insets(0, 5, 5, 5);
        add(edPath, c);
        pack();
        
        btGetPath = new JButton();
        btGetPath.setText("...");
        c = new GridBagConstraints();
        c.anchor = GridBagConstraints.LINE_END;
        c.gridx = 1;
        c.gridy = 1;
        c.insets = new Insets(0, 0, 5, 5);
        //btGetPath.setPreferredSize(new Dimension(35, edPath.getHeight()));
        add(btGetPath, c);
        
        btValidate = new JButton();
        btValidate.setText("Validate");
        c = new GridBagConstraints();
        c.anchor = GridBagConstraints.LINE_START;
        c.gridx = 0;
        c.gridy = 2;
        c.insets = new Insets(0, 5, 5, 5);
        add(btValidate, c);

        // Set the actions
        btGetPath.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent event) {
        		selectDocument();
        	}
        });
        btValidate.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent event) {
        		validateDocument();
        	}
        });

        
        edLog = new JTextPane();
        edLog.setEditable(false);
        edLog.setBorder(BorderFactory.createLineBorder(Color.gray));
        c = new GridBagConstraints();
        c.anchor = GridBagConstraints.LINE_START;
        c.gridx = 0; c.gridwidth = 2;
        c.gridy = 3;
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 1.0;
        c.weighty = 1.0;
        c.insets = new Insets(0, 5, 5, 5);
        add(edLog, c);

    	lbPath = new JLabel("For additional functions use Lynx in command-line mode (-h option to see all commands)");
        c = new GridBagConstraints();
        c.anchor = GridBagConstraints.LAST_LINE_START;
        c.gridx = 0; c.gridwidth = 2;
        c.gridy = 4;
        c.insets = new Insets(0, 5, 5, 5);
    	add(lbPath, c);
    	
        // Set minimum and preferred size for the dialog
        setMinimumSize(new Dimension(550, 250));
        setPreferredSize(new Dimension(650, 350));
        pack();
        
        // Set the drag and drop handlers
        final DocumentTransferhandler dth = new DocumentTransferhandler();
    	setTransferHandler(dth);
    	edLog.setTransferHandler(dth);

        // Center the dialog
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation((dim.width-getSize().width)/2, (dim.height-getSize().height)/2);
    }
 
    private void selectDocument () {
    	try {
    		JFileChooser fc = new JFileChooser();
    		fc.setDialogTitle("Select an XLIFF 2 Document");
    		int option = fc.showOpenDialog(this);
    		if ( option == JFileChooser.APPROVE_OPTION ) {
   				edPath.setText(fc.getSelectedFile().getAbsolutePath());
    		}
    	}
    	catch ( Throwable e ) {
    		log(e.getLocalizedMessage());
    	}
    }
    
    private void validateDocument () {
    	edLog.setText("");
		XLIFFReader reader = null;
    	try {
    		String path = edPath.getText().trim();
    		File file = new File(path);
    		// Schema validation
    		log("Validating against the schemas...");
			// Processing validation
			reader = new XLIFFReader(XLIFFReader.VALIDATION_MAXIMAL);
			reader.open(file);
			log("Schemas validation successful.");
			log("Validating core processing requirements...");
			while ( reader.hasNext() ) {
				reader.next();
			}
			log("Core processing validation successful.");
    	}
    	catch ( Throwable e ) {
    		log(e.getLocalizedMessage());
    	}
		finally {
			if ( reader != null ) reader.close();
			reader = null;
		}
    }
    
    private void log (String text) {
    	edLog.setText(edLog.getText()+text+"\n");
    }
    
    public static void start () {
        java.awt.EventQueue.invokeLater(new Runnable() {
        	@Override
            public void run() {
                new MainDialog().setVisible(true);
            }
        });
    }
}
