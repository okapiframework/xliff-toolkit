/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.applications.lynxweb;

public class Html {

	static public long FILESIZEMAX = 51200; // 50Kb
	static public long SIZEMAX = 54272; // 53Kb
	
	static public String HEAD1 = "<html><head>"
		+ "<meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>"
		+ "<title>Okapi Lynx-Web</title>"
		+ "<style type='text/css'>body { font-family: Arial, Helvetica, sans-serif; }</style>"
		+ "<head><body><h1>";
	
	static public String HEAD2 = "</h2>"
		+ "<table style='padding-bottom:0.5em'><tr><td rowspan=2 style='text-align:left;vertical-align:top;'>[&nbsp;<a href='/'>Home</a>&nbsp;]&nbsp;&nbsp;<small>Notes:</small></td>"
		+ "<td><small>- XLIFF 2.0 is an <b>OASIS Standard</b>. The processing performed here "
		+ "should correspond to <a target='_blank' href='http://docs.oasis-open.org/xliff/xliff-core/v2.0/xliff-core-v2.0.html'>the latest version of the specification</a>.</small></td></tr>"
		+ "<tr><td><small>- Schema validation: Core and all modules, processing requirements/constraints validation: "
		+ "Core, Translation Candidates, Glossary, Metadata and Validation.<small></td></tr>"
		+ "</table>";
	
	static public String INFO = "<ul>"
		+ "<li>This application uses <a target='_blank' href='https://bitbucket.org/okapiframework/xliff-toolkit'>the Okapi XLIFF Toolkit</a>.</li>"
		+ "<li>Documents size is limited to about 50Kb on-line, please use the command-line version of Lynx for larger documents.</li>"
		+ "<li>Report issues at <a target='_blank' href='https://bitbucket.org/okapiframework/xliff-toolkit/issues'>https://bitbucket.org/okapiframework/xliff-toolkit/issues</a></li>"
		+ "<li>For more information about XLIFF see <a target='_blank' href='http://www.oasis-open.org/committees/xliff'>the OASIS XLIFF TC page</a>.</li>"
		+ "</ul></body></html>";
	
}
